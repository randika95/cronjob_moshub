 //use path module
const path = require('path');
//use express module
const express = require('express');
//use hbs view engine
const hbs = require('hbs');
//use bodyParser middleware
const bodyParser = require('body-parser');
//use mysql database
const mysql = require('mysql');
const app = express();

const {Client}=require('pg');

const empty=null;


//konfigurasi koneksi db mysql local
const conn = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'react'
});


// konfigurasi koneksi db konso
const connection_moshub_konso=new Client({
  host: 'dbs-konsolidasi.emos.id',
  user: 'moshub_konso',
  password: 'konsomoshub',
  port:'39725',
  database: 'dbskonso'

});


// konfigurasi koneksi db moshub production
const connection_moshub=new Client({
  host: 'moshub-rds-prod-postgre-01.cjn7lgjsj9cm.ap-southeast-1.rds.amazonaws.com',
  user: 'moshub_dev_lusius',
  password: 'bTA1aHUxMzRkbTFu',
  port:'5432',
  database: 'moshub_enseval_b2c'

});


// konfigurasi koneksi db moshub development
const connection_moshub_dev=new Client({
  host: 'moshub-rds-prod-postgre-01.cjn7lgjsj9cm.ap-southeast-1.rds.amazonaws.com',
  user: 'moshub_dev_lusius',
  password: 'bTA1aHUxMzRkbTFu',
  port:'5432',
  database: 'moshub_enseval_b2c_dev'

});

connection_moshub_konso.connect((err) =>{
  if(err) throw err;
  //console.log('Postgree Connected to DB Postgree DB Moshub Konsolidasi');
});

connection_moshub.connect((err) =>{
  if(err) throw err;
  //console.log('Postgree Connected to DB Postgree DB Moshub Lusius');
});

 


const cors=require("cors");
const corsOptions ={
   origin:'*',
   credentials:true,            //access-control-allow-credentials:true
   optionSuccessStatus:200,
}

app.use(cors(corsOptions))
 
//connect ke database
conn.connect((err) =>{
  if(err) throw err;
  //console.log('Mysql Connected to DB');
});

// parse application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// parse application/json
app.use(express.json());


var cron = require('node-cron');

// merubah type datetime from UTC format to datetme dengan string format

function currentTime(date){

  var datetime = date.getFullYear() + "-"+(date.getMonth()+1) 
      + "-" +date.getDate() + " " 
      + date.getHours() + ":" 
      + date.getMinutes() + ":" + date.getSeconds();

  return datetime;

}



function retrievedata(result){
  var count = Object.keys(result.rows).length;
  var order_datetime;
  var process_datetime;
  var request_pickup_datetime;
  var shipping_datetime;
  var arrival_datetime;
  var complete_datetime;
  var cancel_datetime;
  var currentdatenow = new Date();
 
  for (i=0;i<count;i++)
  {
      // For SimulationTesting
      //console.log(typeof result.rows[i].order_date);
      //var nul="'2020-07-20 10:10:10'";
      //console.log(typeof nul);
      //console.log("order date :"+result.rows[i].order_date);


      var create_datetime=currentTime(currentdatenow);
      var create_by="system";
      var update_datetime=currentTime(currentdatenow);
      var update_by="system";
      sql="INSERT INTO order_sla_moshub (shop_name,partner_name,branch_code,marketplace_order_id,invoice_number,marketplace_name,cancel_reason,status_order,buyer_id,buyer_name,sku,product_name,qty,item_price,item_price_total,order_price_total,destination_address,provice_name,city_name,district_name,postal_code,order_date,process_date,request_pickup_date,shipping_date,arrival_date,complete_date,cancel_date,create_date,create_by,update_date,update_by) "+
      "values('"+result.rows[i].shop_name+"','"+result.rows[i].partner_name+"','"+result.rows[i].branch_code+"','"+result.rows[i].marketplace_order_id+"','"+result.rows[i].invoice_number+"','"+result.rows[i].marketplace+"','"+result.rows[i].cancel_reason+"','"+result.rows[i].status_order+"','"+result.rows[i].buyer_id+"','"+result.rows[i].buyer_name+"','"+result.rows[i].sku+"','"+result.rows[i].product_name+"','"+result.rows[i].qty+"','"+result.rows[i].item_price+"','"+result.rows[i].item_price_total+"','"+result.rows[i].order_price_total+"','"+result.rows[i].destination_address+"','"+result.rows[i].provice_name+"','"+result.rows[i].city_name+"','"+result.rows[i].district_name+"','"+result.rows[i].postal_code+"',"+convertToDatetime(result.rows[i].order_date)+","+convertToDatetime(result.rows[i].process_date)+","+convertToDatetime(result.rows[i].request_pickup_date)+","+convertToDatetime(result.rows[i].shipping_date)+","+convertToDatetime(result.rows[i].arrival_date)+","+convertToDatetime(result.rows[i].complete_date)+","+convertToDatetime(result.rows[i].cancel_date)+",'"+create_datetime+"','system','"+update_datetime+"','system')"+
      "on conflict(invoice_number,sku) DO UPDATE SET status_order=EXCLUDED.status_order, order_date = EXCLUDED.order_date, process_date=EXCLUDED.process_date, request_pickup_date=EXCLUDED.request_pickup_date, shipping_date=EXCLUDED.shipping_date, arrival_date=EXCLUDED.arrival_date , complete_date=EXCLUDED.complete_date, create_date=EXCLUDED.create_date, update_date=EXCLUDED.update_date , cancel_date=EXCLUDED.cancel_date"
      ";";
      connection_moshub_konso.query(sql, function (err, result, fields) {
      if (err) throw err;
       //console.log("Inserted data moshub.");
      }); 

  }
}



function convertToDatetime(waktu){

  if(waktu!=null)
  {
      //var currentdatenow = new Date();
      //console.log("sekarang :"+currentdatenow.toISOString());
      let unix_timestamp = waktu;
      // Create a new JavaScript Date object based on the timestamp
      // multiplied by 1000 so that the argument is in milliseconds, not seconds.
      //const str = new Date().toLocaleString('en-US', { timeZone: 'Asia/Jakarta' });
      //console.log("local:"str);
      //var dt = new Date('2021-07-24T20:37:26.007' + 'Z');
      //console.log("local :"+dt.toLocaleString('en-US', { timeZone: 'Asia/Jakarta' }));
       
      var date = new Date(unix_timestamp);
      var years=date.getFullYear(); // get years data
      var month=date.getMonth()+1; // get month month
      var tgl=date.getDate(); // get tgl
      // Get Data Hours part from the timestamp
      var hours = date.getHours()-7;
      // Get Minutes part from the timestamp
      var minutes = "0" + date.getMinutes();
      // Get Seconds part from the timestamp
      var seconds = "0" + date.getSeconds();
      // Assign all datetime to variable formattedTime
      var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
      //console.log(formattedTime);
      var full_time= "'"+years+"-"+month+"-"+tgl+" "+formattedTime+"'";

      return full_time;
      
  }else
  {
      // empty value is null and it was const type
      return empty;
  }
  
}


// route for testing upsert
app.get('/upsert', (req, res) => {
  //console.log("testing");
  sql="INSERT INTO order_sla_moshub (shop_name,partner_name,branch_code,marketplace_order_id,invoice_number,marketplace_name,cancel_reason,status_order,buyer_id,buyer_name,sku,product_name,qty,item_price,item_price_total,order_price_total,destination_address,provice_name,city_name,district_name,postal_code,order_date,process_date,request_pickup_date,shipping_date,arrival_date,complete_date,cancel_date,create_date,create_by,update_date,update_by) "+
      "values('Mitrasana Bogor','MITRASANA','BGR','965035567','INV/20211018/MPL/1680631507','TOKOPEDIA','','DIPROSES','13563465','Syahru Ramdhoni','SMLLD','MELOLIN 10 X 10CM/1X100','3','10447','31341','31341','Ciomas Rahayu, Ciomas, B…T 02/10 Blok P10 no 16]','JAWA BARA','KAB. BOGOR','CIOMAS','16610','2021-10-18 00:21:58','2021-10-18 00:21:58','2021-10-18 00:21:58','2021-10-18 00:21:58','2021-10-18 00:21:58','2021-10-18 00:21:58','2021-10-18 00:21:58','2021-10-18 00:21:58','system','2021-10-18 00:21:58','system')"+
      "on conflict(invoice_number,sku) DO UPDATE SET status_order=EXCLUDED.status_order, order_date = EXCLUDED.order_date, process_date=EXCLUDED.process_date, request_pickup_date=EXCLUDED.request_pickup_date, shipping_date=EXCLUDED.shipping_date, arrival_date=EXCLUDED.arrival_date , complete_date=EXCLUDED.complete_date, create_date=EXCLUDED.create_date, update_date=EXCLUDED.update_date , cancel_date=EXCLUDED.cancel_date"
      ";";
      connection_moshub_konso.query(sql, function (err, result, fields) {
      if (err) throw err;
       //console.log("Inserted data moshub.");
      }); 


});



// route for first populate
app.get('/first_populate', (req, res) => {

  var querysql="select"+
" mu.shop_name,"+
" mp.partner_name partner_name,"+
" mu.branch_id branch_code,"+
" sit.shop_id,"+
" moh.marketplace_order_id marketplace_order_id,"+ 
" moh.marketplace_invoice_number invoice_number,"+ 
" mm.marketplace_name marketplace,"+ 
" upper(msob.status_order_name) status_order,"+
" moh.buyer_id,"+ 
" moh.buyer_name,"+ 
" mpu.sku,"+
" moh.cancel_reason,"+ 
" mpu.product_name, "+
" mod2.qty,"+
" mod2.price item_price,"+ 
" mod2.price_total item_price_total,"+
" moh.price_total order_price_total,"+
" moh.destination_address,"+
" upper(moh.destination_provice_name) provice_name,"+
" upper(moh.destination_city_name) city_name,"+
" upper(moh.destination_district_name) district_name,"+
" upper(moh.destination_postal_code) postal_code,"+
" cancel_date.create_date cancel_date,"+
" order_date.create_date as order_date,"+
" process_date.create_date as process_date,"+
" request_pickup_date.create_date as request_pickup_date,"+
" shipping_date.create_date as shipping_date,"+
" arrival_date.create_date as arrival_date,"+
" complete_date.create_date as complete_date"+
" from "+
" moshub_order_detail mod2"+  
" left join moshub_order_header moh on moh.moshub_order_id = mod2.moshub_order_id "+
" left join master_user_partner mup on mup.id_user = moh.moshub_user_id"+
" left join master_partner mp on mp.id = mup.id_partner "+
" left join master_status_order_moshub msob on msob.status_order_code = moh.moshub_status_order "+
" left join master_marketplace mm on mm.id = moh.marketplace_id::numeric "+
" left join master_product_user mpu on mpu.id = mod2.moshub_product_id::numeric "+
" left join master_user mu on mu.id = moh.moshub_user_id"+
" left join moshub_master_logistic mml on mml.id = moh.moshub_logistic_id::numeric "+
" left join shop_info_tokopedia sit on sit.id_user = moh.moshub_user_id "+
" left join "+
" (select order_id_moshub, create_date from moshub_order_status_history mosh where status = '200') as order_date on order_date.order_id_moshub = moh.moshub_order_id"+
" left join "+
" (select order_id_moshub, create_date from moshub_order_status_history mosh where status = '220') as process_date on process_date.order_id_moshub = moh.moshub_order_id"+
" left join "+
" (select order_id_moshub, create_date from moshub_order_status_history mosh where status = '300') as request_pickup_date on request_pickup_date.order_id_moshub = moh.moshub_order_id"+
" left join "+
" (select order_id_moshub, create_date from moshub_order_status_history mosh where status = '400') as shipping_date on shipping_date.order_id_moshub = moh.moshub_order_id"+
" left join"+ 
" (select order_id_moshub, create_date from moshub_order_status_history mosh where status = '500') as arrival_date on arrival_date.order_id_moshub = moh.moshub_order_id"+
" left join "+
" (select order_id_moshub, create_date from moshub_order_status_history mosh where status = '600') as complete_date on complete_date.order_id_moshub = moh.moshub_order_id"+
" left join "+
" (select order_id_moshub, create_date from moshub_order_status_history mosh where status = '230') as cancel_date on cancel_date.order_id_moshub = moh.moshub_order_id"+
" order by moh.create_date"
;
   
     connection_moshub.query(querysql, function (err, result, fields) {
     if (err) throw err;
     res.send(result.rows);
     retrievedata(result);
     //console.log(result.rows);
     //console.log(result.fields);
    }); 

  
});


// cronjob for populate data every 1 pm

cron.schedule('0 1 * * *', () => {

  var querysql="select"+
" mu.shop_name,"+
" mp.partner_name partner_name,"+
" mu.branch_id branch_code,"+
" moh.marketplace_order_id marketplace_order_id,"+ 
" moh.marketplace_invoice_number invoice_number,"+ 
" mm.marketplace_name marketplace,"+ 
" upper(msob.status_order_name) status_order,"+
" moh.buyer_id,"+ 
" moh.buyer_name,"+ 
" mpu.sku,"+
" moh.cancel_reason,"+ 
" mpu.product_name, "+
" mod2.qty,"+
" mod2.price item_price,"+ 
" mod2.price_total item_price_total,"+
" moh.price_total order_price_total,"+
" moh.destination_address,"+
" upper(moh.destination_provice_name) provice_name,"+
" upper(moh.destination_city_name) city_name,"+
" upper(moh.destination_district_name) district_name,"+
" upper(moh.destination_postal_code) postal_code,"+
" cancel_date.create_date cancel_date,"+
" order_date.create_date as order_date,"+
" process_date.create_date as process_date,"+
" request_pickup_date.create_date as request_pickup_date,"+
" shipping_date.create_date as shipping_date,"+
" arrival_date.create_date as arrival_date,"+
" complete_date.create_date as complete_date,"+
" moh.update_date"+
" from "+
" moshub_order_detail mod2"+  
" left join moshub_order_header moh on moh.moshub_order_id = mod2.moshub_order_id "+
" left join master_user_partner mup on mup.id_user = moh.moshub_user_id"+
" left join master_partner mp on mp.id = mup.id_partner "+
" left join master_status_order_moshub msob on msob.status_order_code = moh.moshub_status_order "+
" left join master_marketplace mm on mm.id = moh.marketplace_id::numeric "+
" left join master_product_user mpu on mpu.id = mod2.moshub_product_id::numeric "+
" left join master_user mu on mu.id = moh.moshub_user_id"+
" left join moshub_master_logistic mml on mml.id = moh.moshub_logistic_id::numeric "+
" left join shop_info_tokopedia sit on sit.id_user = moh.moshub_user_id "+
" left join "+
" (select order_id_moshub, create_date from moshub_order_status_history mosh where status = '200') as order_date on order_date.order_id_moshub = moh.moshub_order_id"+
" left join "+
" (select order_id_moshub, create_date from moshub_order_status_history mosh where status = '220') as process_date on process_date.order_id_moshub = moh.moshub_order_id"+
" left join "+
" (select order_id_moshub, create_date from moshub_order_status_history mosh where status = '300') as request_pickup_date on request_pickup_date.order_id_moshub = moh.moshub_order_id"+
" left join "+
" (select order_id_moshub, create_date from moshub_order_status_history mosh where status = '400') as shipping_date on shipping_date.order_id_moshub = moh.moshub_order_id"+
" left join"+ 
" (select order_id_moshub, create_date from moshub_order_status_history mosh where status = '500') as arrival_date on arrival_date.order_id_moshub = moh.moshub_order_id"+
" left join "+
" (select order_id_moshub, create_date from moshub_order_status_history mosh where status = '600') as complete_date on complete_date.order_id_moshub = moh.moshub_order_id"+
" left join "+
" (select order_id_moshub, create_date from moshub_order_status_history mosh where status = '230') as cancel_date on cancel_date.order_id_moshub = moh.moshub_order_id"+
" where "+
" moh.update_date <= now() - interval '1 hour' "+
" and "+
" moh.update_date >= now() - interval '24 hour' "+ 
" order by moh.update_date desc"
;
   
     connection_moshub.query(querysql, function (err, result, fields) {
     if (err) throw err;
     //res.send(result.rows);
     retrievedata(result);
     //console.log(result.rows);
     //console.log(result.fields);
    }); 

  
});






//server listening
app.listen(8000, () => {
  //console.log('Server is running at port 8000');
});
